package com.webstore.domain.json;

/**
 * Created by SGurin on 01.08.2016.
 */
public class FilterJson {
   private int id;
   private int cnt;

    public FilterJson() {
    }

    public FilterJson(int id, int cnt) {
        this.id = id;
        this.cnt = cnt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }
}
